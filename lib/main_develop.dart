import 'package:desafiosuperherois/app/app_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_stetho/flutter_stetho.dart';
import 'package:global_configuration/global_configuration.dart';

void main() async{

  WidgetsFlutterBinding.ensureInitialized();

  await GlobalConfiguration().loadFromPath("assets/config/development.json");
  await GlobalConfiguration().loadFromPath("assets/config/shared.json");
  Stetho.initialize();
  return runApp(ModularApp(module: AppModule()));
}
