import 'package:desafiosuperherois/app/app_controller.dart';
import 'package:desafiosuperherois/app/app_widget.dart';
import 'package:desafiosuperherois/app/modules/detail/detail_module.dart';
import 'package:desafiosuperherois/app/modules/home/home_module.dart';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'modules/home/model/heros.dart';
import 'modules/share/loggin_interceptor.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => Heros()),
        Bind((i) => Dio()..interceptors.add(i.get<LogginInterceptor>())),
        Bind((i) => LogginInterceptor()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, module: HomeModule(),transition: TransitionType.fadeIn,),
        Router("/detail", module: DetailModule(),transition: TransitionType.fadeIn,),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
