// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$searchAtom = Atom(name: '_HomeControllerBase.search');

  @override
  bool get search {
    _$searchAtom.reportRead();
    return super.search;
  }

  @override
  set search(bool value) {
    _$searchAtom.reportWrite(value, super.search, () {
      super.search = value;
    });
  }

  final _$errorAtom = Atom(name: '_HomeControllerBase.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$statusAtom = Atom(name: '_HomeControllerBase.status');

  @override
  Resource<dynamic> get status {
    _$statusAtom.reportRead();
    return super.status;
  }

  @override
  set status(Resource<dynamic> value) {
    _$statusAtom.reportWrite(value, super.status, () {
      super.status = value;
    });
  }

  final _$getHerosAsyncAction = AsyncAction('_HomeControllerBase.getHeros');

  @override
  Future getHeros(int id) {
    return _$getHerosAsyncAction.run(() => super.getHeros(id));
  }

  final _$findHeroAsyncAction = AsyncAction('_HomeControllerBase.findHero');

  @override
  Future findHero(String text) {
    return _$findHeroAsyncAction.run(() => super.findHero(text));
  }

  final _$_HomeControllerBaseActionController =
      ActionController(name: '_HomeControllerBase');

  @override
  dynamic changeError(bool value) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.changeError');
    try {
      return super.changeError(value);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeSearch(bool value) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.changeSearch');
    try {
      return super.changeSearch(value);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic close() {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.close');
    try {
      return super.close();
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
search: ${search},
error: ${error},
status: ${status}
    ''';
  }
}
