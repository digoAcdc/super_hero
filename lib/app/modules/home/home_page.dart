import 'package:desafiosuperherois/app/modules/home/components/item_hero.dart';
import 'package:desafiosuperherois/app/modules/home/model/resource.dart';
import 'package:desafiosuperherois/app/modules/share/error_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mobx/mobx.dart';

import 'components/scaffold_search_title.dart';
import 'components/scaffold_title.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;

  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );

  @override
  void initState() {
    controller.getHeros(0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _switchScaffoldTitle(controller),
        actions: <Widget>[
          IconButton(
              icon: actionIcon,
              onPressed: () {
                setState(() {
                  if (this.actionIcon.icon == Icons.search) {
                    this.actionIcon = new Icon(
                      Icons.close,
                      color: Colors.white,
                    );
                  } else {
                    controller.close();
                    this.actionIcon = Icon(
                      Icons.search,
                      color: Colors.white,
                    );
                  }
                });
              }),
        ],
      ),
      body: _body(),
    );
  }

  _switchScaffoldTitle(HomeController controller) {
    if (this.actionIcon.icon == Icons.search)
      return ScaffoldTitle();
    else {
      return ScaffoldSearchTitle(
        homeController: controller,
      );
    }
  }

  _body() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return SafeArea(
          child: Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Observer(
                      builder: (BuildContext context) {
                        return Container(
                          height: constraints.maxHeight,
                          child: StaggeredGridView.countBuilder(
                            crossAxisCount: 4,
                            itemCount: controller.search || controller.error
                                ? controller.heros.length
                                : controller.heros.length + 1,
                            itemBuilder: (BuildContext context, int index) {
                              if (index < controller.heros.length) {
                                return ItemHero(
                                  hero: controller.heros[index],
                                  index: index,
                                );
                              } else {
                                getMoreData();
                                return Center(
                                    child: CircularProgressIndicator());
                              }
                            },
                            staggeredTileBuilder: (int index) =>
                                new StaggeredTile.count(
                                    2, index.isEven ? 2 : 1),
                            mainAxisSpacing: 4.0,
                            crossAxisSpacing: 4.0,
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              Observer(
                builder: (BuildContext context) {
                  if (controller.error)
                    return ErrorFlare();
                  else
                    return Container();
                },
              )
            ],
          ),
        );
      },
    );
  }

  void getMoreData() {
    controller.getHeros(controller.heros.length + 1);
  }
}
