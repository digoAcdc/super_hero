import 'package:desafiosuperherois/app/modules/home/model/resource.dart';
import 'package:desafiosuperherois/app/modules/home/model/search_hero.dart';
import 'package:desafiosuperherois/app/modules/home/model/super_hero.dart';
import 'package:desafiosuperherois/app/modules/home/repository/home_repository_interface.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:global_configuration/global_configuration.dart';

class HomeRepository implements IHomeRepository {
  Dio dio = Modular.get<Dio>();

  @override
  Future<Resource> getSuperHeroes(int id) async {
    try {
      Response response = await dio
          .get("/${GlobalConfiguration().getString('KEY_SUPER_HERO')}/${id}");

      if (response.statusCode >= 200 && response.statusCode <= 299) {
        SuperHero hero = SuperHero.fromJson(response.data);
        if (hero.response == "success") {
          Resource r = Resource();
          r.data = hero;
          r.stateEnum = ResourceStateEnum.SUCCESS;
          return r;
        }
      }
    } on DioError catch (e) {
      String erro = "";
    }
  }

  @override
  Future<Resource> searchSuperHeroes(String text) async {
    try {
      Response response = await dio.get(
          "/${GlobalConfiguration().getString('KEY_SUPER_HERO')}/search/${text}");

      if (response.statusCode >= 200 && response.statusCode <= 299) {
        SearchHero hero = SearchHero.fromJson(response.data);
        if (hero.response == "success") {
          Resource r = Resource();
          r.data = hero;
          r.stateEnum = ResourceStateEnum.SUCCESS;
          return r;
        }
      }
    } on DioError catch (e) {
      String erro = "";
    }
  }
}
