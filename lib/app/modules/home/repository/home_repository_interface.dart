
import 'package:desafiosuperherois/app/modules/home/model/resource.dart';

abstract class IHomeRepository {

  Future<Resource> getSuperHeroes(int id);
  Future<Resource> searchSuperHeroes(String text);

}