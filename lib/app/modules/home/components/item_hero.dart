import 'package:desafiosuperherois/app/modules/components/catergory_powerstats.dart';
import 'package:desafiosuperherois/app/modules/home/model/super_hero.dart';
import 'package:desafiosuperherois/app/modules/share/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class ItemHero extends StatelessWidget {
  final int index;
  final SuperHero hero;

  ItemHero({Key key, @required this.index, @required this.hero})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Modular.to.pushNamed("/detail", arguments: hero);
      },
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Stack(
            children: <Widget>[
              Container(
                child: new Center(
                  child: AspectRatio(
                    aspectRatio: index.isEven ? 1 : 2,
                    child: Hero(
                      tag: "${hero.id}image",
                      child: FadeInImage.assetNetwork(
                        fit: BoxFit.cover,
                        imageErrorBuilder: (um, dois, tres) {
                          return Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.error,
                                  color: Colors.red,
                                ),
                                Text(
                                  "Falha ao carregar",
                                  style: TextStyle(color: Colors.red),
                                )
                              ],
                            ),
                          );
                        },
                        image: hero.image.url,
                        placeholder: "",
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  padding: EdgeInsets.only(top: 4, bottom: 4),
                  width: constraints.maxWidth,
                  color: Colors.black,
                  child: Text(
                    hero.name,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              Positioned(
                bottom: 16,
                right: 8,
                child: GestureDetector(
                  onTap: () {
                    _showDialog(context);
                  },
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: new BoxDecoration(
                        color: colorPrimary.withOpacity(0.7),
                        borderRadius:
                            new BorderRadius.all(Radius.circular(50.0))),
                    child: Icon(
                      Icons.offline_bolt,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  _showDialog(BuildContext context) {
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            elevation: 30,

            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            //this right here
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.all(16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "powerstats",
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CategoryPowerStats(
                      hero: hero,
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
