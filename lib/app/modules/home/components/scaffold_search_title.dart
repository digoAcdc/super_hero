import 'package:desafiosuperherois/app/modules/home/home_controller.dart';
import 'package:desafiosuperherois/app/modules/share/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ScaffoldSearchTitle extends StatefulWidget {
  final TextEditingController searchQuery = TextEditingController();
  final Function(String) onchange;
  final HomeController homeController;

  ScaffoldSearchTitle({this.onchange, @required this.homeController});

  int currentValue;

  @override
  _ScaffoldSearchTitleState createState() => _ScaffoldSearchTitleState();
}

class _ScaffoldSearchTitleState extends State<ScaffoldSearchTitle> {
  TextEditingController get searchQuery => widget.searchQuery;

  HomeController get homeController => widget.homeController;

  Function(String) get onchange => widget.onchange;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      color: colorPrimaryMaterial,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(
                width: 4,
              ),
              Flexible(
                flex: 1,
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      if (searchQuery.text.trim().length > 0)
                        homeController.findHero(searchQuery.text.toUpperCase());
                    },
                    child: new Icon(Icons.search, color: Colors.white),
                  ),
                ),
              ),
              Flexible(
                flex: 4,
                child: TextField(
                  textInputAction: TextInputAction.search,
                  onSubmitted: (t) {
                    if (searchQuery.text.trim().length > 0)
                      homeController.findHero(searchQuery.text.toUpperCase());
                  },
                  style: new TextStyle(
                    color: Colors.white,
                  ),
                  decoration: InputDecoration(
                      hintText: "Pesquisar",
                      hintStyle: new TextStyle(color: Colors.white)),
                  onChanged: onchange,
                  controller: searchQuery,
                ),
              ),
              SizedBox(
                width: 4,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
