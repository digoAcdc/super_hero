import 'package:flutter/material.dart';

class ScaffoldTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Super Heróis",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ],
    );
  }
}
