import 'package:desafiosuperherois/app/modules/home/home_controller.dart';
import 'package:desafiosuperherois/app/modules/home/home_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'repository/home_repository.dart';
import 'repository/home_repository_interface.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
    Bind<IHomeRepository>((i) => HomeRepository(), singleton: false),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => HomePage()),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
