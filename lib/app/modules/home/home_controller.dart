import 'package:desafiosuperherois/app/modules/home/model/search_hero.dart';
import 'package:desafiosuperherois/app/modules/home/repository/home_repository_interface.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import 'model/heros.dart';
import 'model/resource.dart';
import 'model/super_hero.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  ObservableList<SuperHero> heros = ObservableList<SuperHero>();

  @observable
  bool search = false;

  @observable
  bool error = false;

  @action
  changeError(bool value) {
    error = value;
    print(value);
  }

  int lastIndex = 0;

  @observable
  Resource status = Resource(stateEnum: ResourceStateEnum.NONE);

  @action
  changeSearch(bool value) => search = value;

  @action
  close() {
    Heros globalHeros = Modular.get<Heros>();
    heros.clear();
    heros.addAll(globalHeros.heros);
    changeSearch(false);
    getHeros(globalHeros.heros.length + 1);
  }

  @action
  getHeros(int id) async {
    try {
      if (!search) {
        if (id != lastIndex && id != 0) {
          IHomeRepository repository = Modular.get<IHomeRepository>();

          Resource resource = await repository.getSuperHeroes(id);

          switch (resource.stateEnum) {
            case ResourceStateEnum.SUCCESS:
              SuperHero superHero = resource.data;
              Heros h = Modular.get<Heros>();

              h.heros.add(superHero);
              heros.add(superHero);
              break;
            case ResourceStateEnum.ERROR:
              break;
            case ResourceStateEnum.NONE:
              // TODO: Handle this case.
              break;
          }
        }
      }
    } catch (e) {
      error = true;
    }
  }

  @action
  findHero(String text) async {
    try {
      changeSearch(true);

      heros.clear();

      IHomeRepository repository = Modular.get<IHomeRepository>();

      Resource resource = await repository.searchSuperHeroes(text.trim());

      switch (resource.stateEnum) {
        case ResourceStateEnum.SUCCESS:
          try {
            SearchHero searchHero = resource.data;
            heros.addAll(searchHero.results);
          } catch (e) {
            String teste = "";
          }
          break;
        case ResourceStateEnum.ERROR:
          break;
        case ResourceStateEnum.NONE:
          // TODO: Handle this case.
          break;
      }
    } catch (e) {
      error = true;
    }
  }
}
