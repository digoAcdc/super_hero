import 'dart:convert';

import 'package:desafiosuperherois/app/modules/home/model/super_hero.dart';

SearchHero searchHeroFromJson(String str) => SearchHero.fromJson(json.decode(str));

String searchHeroToJson(SearchHero data) => json.encode(data.toJson());

class SearchHero {
  SearchHero({
    this.response,
    this.resultsFor,
    this.results,
  });

  String response;
  String resultsFor;
  List<SuperHero> results;

  factory SearchHero.fromJson(Map<String, dynamic> json) => SearchHero(
    response: json["response"] == null ? null : json["response"],
    resultsFor: json["results-for"] == null ? null : json["results-for"],
    results: json["results"] == null ? null : List<SuperHero>.from(json["results"].map((x) => SuperHero.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "response": response == null ? null : response,
    "results-for": resultsFor == null ? null : resultsFor,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
  };
}