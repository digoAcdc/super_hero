class Powerstats {
  Powerstats({
    this.intelligence,
    this.strength,
    this.speed,
    this.durability,
    this.power,
    this.combat,
  });

  String intelligence;
  String strength;
  String speed;
  String durability;
  String power;
  String combat;

  factory Powerstats.fromJson(Map<String, dynamic> json) => Powerstats(
    intelligence: json["intelligence"] == null ? null : json["intelligence"],
    strength: json["strength"] == null ? null : json["strength"],
    speed: json["speed"] == null ? null : json["speed"],
    durability: json["durability"] == null ? null : json["durability"],
    power: json["power"] == null ? null : json["power"],
    combat: json["combat"] == null ? null : json["combat"],
  );

  Map<String, dynamic> toJson() => {
    "intelligence": intelligence == null ? null : intelligence,
    "strength": strength == null ? null : strength,
    "speed": speed == null ? null : speed,
    "durability": durability == null ? null : durability,
    "power": power == null ? null : power,
    "combat": combat == null ? null : combat,
  };
}