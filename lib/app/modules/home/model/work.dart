class Work {
  Work({
    this.occupation,
    this.base,
  });

  String occupation;
  String base;

  factory Work.fromJson(Map<String, dynamic> json) => Work(
    occupation: json["occupation"] == null ? null : json["occupation"],
    base: json["base"] == null ? null : json["base"],
  );

  Map<String, dynamic> toJson() => {
    "occupation": occupation == null ? null : occupation,
    "base": base == null ? null : base,
  };
}