class Image {
  Image({
    this.url,
  });

  String url;

  factory Image.fromJson(Map<String, dynamic> json) => Image(
    url: json["url"] == null ? null : json["url"],
  );

  Map<String, dynamic> toJson() => {
    "url": url == null ? null : url,
  };
}