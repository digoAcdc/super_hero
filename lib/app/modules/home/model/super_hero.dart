// To parse this JSON data, do
//
//     final hero = heroFromJson(jsonString);

import 'dart:convert';

import 'appearance.dart';
import 'biography.dart';
import 'connections.dart';
import 'image.dart';
import 'powerstats.dart';
import 'work.dart';

SuperHero heroFromJson(String str) => SuperHero.fromJson(json.decode(str));

String heroToJson(SuperHero data) => json.encode(data.toJson());

class SuperHero {
  SuperHero({
    this.response,
    this.id,
    this.name,
    this.powerstats,
    this.biography,
    this.appearance,
    this.work,
    this.connections,
    this.image,
  });

  String response;
  String id;
  String name;
  Powerstats powerstats;
  Biography biography;
  Appearance appearance;
  Work work;
  Connections connections;
  Image image;

  factory SuperHero.fromJson(Map<String, dynamic> json) => SuperHero(
    response: json["response"] == null ? null : json["response"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    powerstats: json["powerstats"] == null ? null : Powerstats.fromJson(json["powerstats"]),
    biography: json["biography"] == null ? null : Biography.fromJson(json["biography"]),
    appearance: json["appearance"] == null ? null : Appearance.fromJson(json["appearance"]),
    work: json["work"] == null ? null : Work.fromJson(json["work"]),
    connections: json["connections"] == null ? null : Connections.fromJson(json["connections"]),
    image: json["image"] == null ? null : Image.fromJson(json["image"]),
  );

  Map<String, dynamic> toJson() => {
    "response": response == null ? null : response,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "powerstats": powerstats == null ? null : powerstats.toJson(),
    "biography": biography == null ? null : biography.toJson(),
    "appearance": appearance == null ? null : appearance.toJson(),
    "work": work == null ? null : work.toJson(),
    "connections": connections == null ? null : connections.toJson(),
    "image": image == null ? null : image.toJson(),
  };
}












