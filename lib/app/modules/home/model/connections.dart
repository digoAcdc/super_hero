class Connections {
  Connections({
    this.groupAffiliation,
    this.relatives,
  });

  String groupAffiliation;
  String relatives;

  factory Connections.fromJson(Map<String, dynamic> json) => Connections(
    groupAffiliation: json["group-affiliation"] == null ? null : json["group-affiliation"],
    relatives: json["relatives"] == null ? null : json["relatives"],
  );

  Map<String, dynamic> toJson() => {
    "group-affiliation": groupAffiliation == null ? null : groupAffiliation,
    "relatives": relatives == null ? null : relatives,
  };
}