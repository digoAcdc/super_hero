class Biography {
  Biography({
    this.fullName,
    this.alterEgos,
    this.aliases,
    this.placeOfBirth,
    this.firstAppearance,
    this.publisher,
    this.alignment,
  });

  String fullName;
  String alterEgos;
  List<String> aliases;
  String placeOfBirth;
  String firstAppearance;
  String publisher;
  String alignment;

  factory Biography.fromJson(Map<String, dynamic> json) => Biography(
    fullName: json["full-name"] == null ? null : json["full-name"],
    alterEgos: json["alter-egos"] == null ? null : json["alter-egos"],
    aliases: json["aliases"] == null ? null : List<String>.from(json["aliases"].map((x) => x)),
    placeOfBirth: json["place-of-birth"] == null ? null : json["place-of-birth"],
    firstAppearance: json["first-appearance"] == null ? null : json["first-appearance"],
    publisher: json["publisher"] == null ? null : json["publisher"],
    alignment: json["alignment"] == null ? null : json["alignment"],
  );

  Map<String, dynamic> toJson() => {
    "full-name": fullName == null ? null : fullName,
    "alter-egos": alterEgos == null ? null : alterEgos,
    "aliases": aliases == null ? null : List<dynamic>.from(aliases.map((x) => x)),
    "place-of-birth": placeOfBirth == null ? null : placeOfBirth,
    "first-appearance": firstAppearance == null ? null : firstAppearance,
    "publisher": publisher == null ? null : publisher,
    "alignment": alignment == null ? null : alignment,
  };
}