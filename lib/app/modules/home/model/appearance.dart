class Appearance {
  Appearance({
    this.gender,
    this.race,
    this.height,
    this.weight,
    this.eyeColor,
    this.hairColor,
  });

  String gender;
  String race;
  List<String> height;
  List<String> weight;
  String eyeColor;
  String hairColor;

  factory Appearance.fromJson(Map<String, dynamic> json) => Appearance(
    gender: json["gender"] == null ? null : json["gender"],
    race: json["race"] == null ? null : json["race"],
    height: json["height"] == null ? null : List<String>.from(json["height"].map((x) => x)),
    weight: json["weight"] == null ? null : List<String>.from(json["weight"].map((x) => x)),
    eyeColor: json["eye-color"] == null ? null : json["eye-color"],
    hairColor: json["hair-color"] == null ? null : json["hair-color"],
  );

  Map<String, dynamic> toJson() => {
    "gender": gender == null ? null : gender,
    "race": race == null ? null : race,
    "height": height == null ? null : List<dynamic>.from(height.map((x) => x)),
    "weight": weight == null ? null : List<dynamic>.from(weight.map((x) => x)),
    "eye-color": eyeColor == null ? null : eyeColor,
    "hair-color": hairColor == null ? null : hairColor,
  };
}