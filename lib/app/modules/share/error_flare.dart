import 'package:desafiosuperherois/app/modules/home/home_controller.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class ErrorFlare extends StatelessWidget {
  HomeController homeController = Modular.get<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.black45,
      child: Stack(
        children: <Widget>[
          FlareActor(
            "assets/flare/error_network.flr",
            alignment: Alignment.center,
            artboard: "Artboard",
            animation: "error",
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Text(
                "Falha de conexão",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: GestureDetector(
              onTap: () {
                homeController.changeError(false);
              },
              child: Container(
                margin: EdgeInsets.only(top: 16, right: 16),
                child: Icon(
                  Icons.close,
                  color: Colors.red,
                  size: 40,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
