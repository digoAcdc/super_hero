import 'dart:ui';

import 'package:desafiosuperherois/app/modules/share/HexColor.dart';
import 'package:flutter/material.dart';

Color colorPrimary = HexColor("#35434F");

MaterialColor colorPrimaryMaterial =
    MaterialColor(0xff35434F, const <int, Color>{
  50: const Color(0xff35434F),
  100: const Color(0xff35434F),
  200: const Color(0xff35434F),
  300: const Color(0xff35434F),
  400: const Color(0xff35434F),
  500: const Color(0xff35434F),
  600: const Color(0xff35434F),
  700: const Color(0xff35434F),
  800: const Color(0xff35434F),
  900: const Color(0xff35434F),
});
