import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.black45,
      child: FlareActor(
        "assets/flare/Kid Hero Demo PNG.flr",
        alignment: Alignment.center,
        artboard: "Artboard",
        animation: "Wind",
      ),
    );
  }
}
