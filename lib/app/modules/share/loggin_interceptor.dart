import 'package:dio/dio.dart';
import 'package:global_configuration/global_configuration.dart';

class LogginInterceptor extends InterceptorsWrapper {
  @override
  Future onRequest(RequestOptions options) async {
    options.baseUrl = "${GlobalConfiguration().getString('BASE_URL')}";

    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) {
    return super.onResponse(response);
  }

  @override
  Future onError(DioError error) {
    return super.onError(error);
  }
}
