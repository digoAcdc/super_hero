import 'package:desafiosuperherois/app/modules/components/category_biography.dart';
import 'package:desafiosuperherois/app/modules/components/catergory_powerstats.dart';
import 'package:desafiosuperherois/app/modules/home/model/super_hero.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:transparent_image/transparent_image.dart';

import 'detail_controller.dart';

class DetailPage extends StatefulWidget {
  final String title;
  final SuperHero hero;

  const DetailPage({Key key, this.title = "Detail", @required this.hero})
      : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends ModularState<DetailPage, DetailController> {
  //use 'controller' variable to access controller
  SuperHero get hero => widget.hero;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          hero.name,
          textAlign: TextAlign.center,
        ),
      ),
      body: _body(),
    );
  }

  _body() {
    return ListView(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: 250,
          child: Hero(
            tag: "${hero.id}image",
            child: FadeInImage.memoryNetwork(
              fit: BoxFit.cover,
              placeholder: kTransparentImage,
              image: hero.image.url,
            ),
          ),
        ),
        ExpansionTile(
          initiallyExpanded: true,
          backgroundColor: Colors.grey[200],
          title: Text(
            "POWERSTATS",
            style: TextStyle(color: Colors.black),
          ),
          children: <Widget>[
            CategoryPowerStats(
              hero: hero,
            )
          ],
        ),
        SizedBox(
          height: 16,
        ),
        ExpansionTile(
          initiallyExpanded: true,
          backgroundColor: Colors.grey[200],
          title: Text(
            "BIOGRAPHY",
            style: TextStyle(color: Colors.black),
          ),
          subtitle: Text(
            "Name: ${hero.biography.fullName}",
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
          children: <Widget>[
            CategoryBiography(
              hero: hero,
            )
          ],
        ),
        SizedBox(
          height: 16,
        )
      ],
    );
  }
}
