import 'package:desafiosuperherois/app/modules/detail/detail_controller.dart';
import 'package:desafiosuperherois/app/modules/detail/detail_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class DetailModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => DetailController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => DetailPage(hero: args.data)),
      ];

  static Inject get to => Inject<DetailModule>.of();
}
