import 'package:desafiosuperherois/app/modules/components/item_biography.dart';
import 'package:desafiosuperherois/app/modules/home/model/super_hero.dart';
import 'package:flutter/material.dart';

class CategoryBiography extends StatelessWidget {
  final SuperHero hero;

  const CategoryBiography({Key key, @required this.hero}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16,right: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: ItemBiography(
              name: "alter-egos: ".toUpperCase(),
              description: hero.biography.alterEgos,
            ),
          ),
          ItemBiography(
            name: "aliases: ".toUpperCase(),
            description: _getAliases(),
          ),
          ItemBiography(
            name: "place-of-birth: ".toUpperCase(),
            description: hero.biography.placeOfBirth,
          ),
          ItemBiography(
            name: "first-appearance: ".toUpperCase(),
            description: hero.biography.firstAppearance,
          ),
          ItemBiography(
            name: "publisher: ".toUpperCase(),
            description: hero.biography.publisher,
          ),
          ItemBiography(
            name: "alignment: ".toUpperCase(),
            description: hero.biography.alignment,
          )
        ],
      ),
    );
  }

  _getAliases() {
    var r = new StringBuffer();
    hero.biography.aliases.forEach((element) {r.write("${element} - "); });

    return r.toString();
  }
}
