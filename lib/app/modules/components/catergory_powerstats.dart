import 'package:desafiosuperherois/app/modules/home/model/super_hero.dart';
import 'package:flutter/material.dart';

import 'item_powerstats.dart';

class CategoryPowerStats extends StatelessWidget {
  final SuperHero hero;

  const CategoryPowerStats({Key key,@required this.hero}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return  Column(
      children: <Widget>[
        ItemPowerstats(
          description: "intelligence",
          value: hero?.powerstats?.intelligence == "null" ? 0 : int.parse(hero?.powerstats?.intelligence),
        ),
        ItemPowerstats(
          description: "strength",
          value: hero?.powerstats?.strength == "null" ? 0 : int.parse(hero?.powerstats?.strength),
        ),
        ItemPowerstats(
          description: "speed",
          value: hero?.powerstats?.speed == "null" ? 0 : int.parse( hero?.powerstats?.speed),
        ),
        ItemPowerstats(
          description: "durability",
          value: hero?.powerstats?.durability == "null" ? 0 : int.parse( hero?.powerstats?.durability),
        ),
        ItemPowerstats(
          description: "power",
          value: hero?.powerstats?.power == "null" ? 0 :  int.parse(hero?.powerstats?.power),
        ),
        ItemPowerstats(
          description: "combat",
          value: hero?.powerstats?.combat == "null" ? 0 :  int.parse(hero?.powerstats?.combat),
        ),
      ],
    );
  }
}
