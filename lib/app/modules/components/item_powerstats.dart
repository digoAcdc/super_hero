import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class ItemPowerstats extends StatelessWidget {
  final String description;
  final int value;

  const ItemPowerstats(
      {Key key, @required this.description, @required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4, left: 16, right: 16),
      child: Center(
        child: LinearPercentIndicator(
          animation: true,
          lineHeight: 20.0,
          animationDuration: 2000,
          percent: value / 100.0 > 1 ? 1 : value / 100.0,
          center: Text("${description} - ${value}"),
          linearStrokeCap: LinearStrokeCap.roundAll,
          progressColor: _getColor(),
        ),
      ),
    );
  }

  _getColor() {
    if (value < 33) return Colors.red;

    if (value < 66)
      return Colors.orange;
    else
      return Colors.greenAccent;
  }
}
