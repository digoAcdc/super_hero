import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemBiography extends StatelessWidget {
  final String name;
  final String description;

  const ItemBiography(
      {Key key, @required this.name, @required this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
          text: name,
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w700, fontSize: 18),
          children: <TextSpan>[
            TextSpan(
              text: description,
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
            )
          ]),
    );
  }
}
